import { createStore } from 'vuex'
import VuexPersist from 'vuex-persist'
import JokeService from '../services/JokeService.js'

// setup localstorage key and where to store the persistent state
const vuexPersist = new VuexPersist({
  key: 'DadJokezFavourites',
  storage: window.localStorage
})

export default createStore({
  plugins: [vuexPersist.plugin],
  state: {
    currentJoke: '',
    favouriteJokes: [],
    loading: true
  },
  mutations: { // sync
    setCurrentJoke (state, payload) {
      // store the joke, or error text, in to the state to show the visitor the joke, or an error message
      state.currentJoke = payload
    },
    saveJokeAsFavourite (state, payload) {
      // store payload, the joke copy, in to favouriteJokes array in the state
      state.favouriteJokes.push(payload)
    },
    setLoadingState (state, payload) {
      state.loading = payload
    },
    removeJokeFromFavourite (state, payload) {
      // use filter to check joke id's and only return jokes with id that don't match the payload
      const filterdFavourites = state.favouriteJokes.filter((joke) => {
        return joke.id !== payload.id
      })

      state.favouriteJokes = filterdFavourites
    },
    clearJokes (state) {
      // used to clear the users stored jokes from localstorage
      state.favouriteJokes = []
    }
  },
  actions: { // async
    async loadNewJoke (state) {
      try {
        state.commit('setLoadingState', true)
        const { data } = await JokeService.getJokes()
        if (!data) return
        // set the returned joke and set loading to false to enable the button
        state.commit('setCurrentJoke', data)
      } catch (err) {
        // output the error in to console, but ideally you'd want to log this elsewhere
        console.error('ERROR: ', err)
        // set the current joke to a custom error message for user to see
        state.commit('setCurrentJoke', 'There\'s been an error getting you a new joke, please reload the page')
      } finally {
        state.commit('setLoadingState', false)
      }
    },
    saveJokeAsFavourite (state, joke) {
      // add a single joke to the favourites array
      state.commit('saveJokeAsFavourite', joke)
      state.dispatch('loadNewJoke');
    },
    removeJokeFromFavourite (state, joke) {
      // delete a single joke
      state.commit('removeJokeFromFavourite', joke)
    },
    clearCurrentJokes (state) {
      state.commit('clearJokes')
    }
  },
  getters: {
    // getCurrentJoke (state) {
    //   return state.currentJoke
    // },
    // getFavouriteJokes (state) {
    //   return state.favouriteJokes
    // },
    // getLoadingState (state) {
    //   return state.loading
    // }
    // Can simplify the getters in to arrow functions as we're returning a single object from state
    getCurrentJoke: state => state.currentJoke,
    getFavouriteJokes: state => state.favouriteJokes.reverse(), // reverse the array to show newest at top
    getLoadingState: state => state.loading
  }
})
