import axios from 'axios'

const apiClient = axios.create({
  baseURL: 'https://icanhazdadjoke.com',
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'User-Agent': 'Scripted Pixels (https://bitbucket.org/KamBanwait/vuex-dad-jokes)'
  }
})

export default {
  getJokes () {
    return apiClient.get()
  }
}