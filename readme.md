# 🙄 Dad Jokez

An OTT Vue SPA that uses [Vue3](https://v3.vuejs.org/), [Vuex](https://vuex.vuejs.org/), [Vue Router](https://router.
vuejs.org/), and [Tailwind](https://tailwindcss.com).

A small application that fetches a new joke from `https://icanhazdadjoke.com` and stores a list of all the jokes 
you've read within that session.

You can see the site running here, via Netlify: [Dad has Jokes](https://dadhasjokes.netlify.app)

### How to run locally
- clone repo in to a directory 
- open up in your fav IDE or text editor
- fire up the terminal
- from within the directory you just cloned this code in to run:
    - `npm install` 
    - when that's complete, run `npm run serve`
    - it'll take a few mibutes, depends on your machine/internet speeds, and it'll show the URL to view the app
	- click the url and it'll open in your default browser
- enjoy 😊

### TODO's
- ~~Add localstorage, if supported, to store the jokes locally for users~~
- ~~Handle error if API returns an error~~
- Add 'add to favourites' feature, instead of showing all viewed jokes in 2nd page
- Search favorite jokes?


[![Netlify Status](https://api.netlify.com/api/v1/badges/b3374146-257d-446c-9ad3-c30c0933e32d/deploy-status)](https://app.netlify.com/sites/dadhasjokes/deploys)
